<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DaPostCommentRepository;
use App\Repository\DaPostRepository;
use App\Repository\DaPostCategoryRepository;
use App\Repository\DaUserRepository;
use App\Repository\DaCountryRepository;
use App\Repository\NousContacterRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\DaPostComment;
use App\Entity\NousContacter;
class AdminController extends AbstractController
{
  protected $message;
    protected $nbrMessage;
    protected $nbrComment;
    protected $nbrPost;
    public function __construct(NousContacterRepository $nousContacter,DaPostCommentRepository $comments,DaPostRepository $posts)
    {
      $this->message = $nousContacter->findBy(['isView' => 0,'isDeleted' => 0],['createdAt'  => 'DESC'],3);
      $this->nbrComment = $comments->count(['isApprouved' => 0, 'isDeleted' => 0]);
      $this->nbrMessage = $nousContacter->count(['isDeleted' => 0,'isView' => 0]);
      $this->nbrPost = $posts->count(['isApprouved' => 0, 'isDeleted' => 0]);    
      
    }

    /**
     * @Route("/admin", name="admin")
     */

    public function index(Request $request,DaUserRepository $users,DaPostRepository $daPostRepository,DaCountryRepository $pays,DaPostCategoryRepository $categories): Response
    {

        $posts = $daPostRepository->count(['isDeleted' => 0]);
        $postsDeleted = $daPostRepository->count(['isDeleted' => 1]);
        $pays = $pays->count([]);
        $user = $users->count(['isDeleted' => 0]);

        $journalists = count($users->getJournalist());

        $categories = $categories->count(['isDeleted' => 0]);
        return $this->render('admin/home.html.twig',[ 
            'message' => $this->message,
            'nbr_comm_non_approuve' => $this->nbrComment,
            'nbr_post_non_approuve' => $this->nbrPost,
            'nbr_message_non_lu' => $this->nbrMessage,
            'nbr_post' => $posts,
            'nbr_pays' => $pays,
            'nbr_user' => $user, 
            'nbr_journalist' => $journalists ,
            'nbr_category' => $categories,
            'nbr_deleted_post' => $postsDeleted
          ]);


    }
    /**
     * @Route("/approuve_comment", name="approuve_comment")
     */
   public function adminComment(DaPostCommentRepository $comments)
   {
   		$comments = $comments->findBy(['isApprouved' => 0]);
   		return $this->render('admin/notification/commentaire.html.twig',['comments' => $comments , 'message' => $this->message,'nbr_comm_non_approuve' => $this->nbrComment,'nbr_post_non_approuve' => $this->nbrPost,'nbr_message_non_lu' => $this->nbrMessage]);
   }
   /**
     * @Route("/approuved_comment/{id<[0-9]+>}", name="approuved_comment")
     */
   public function approuveComment(DaPostComment $comments,DaPostCommentRepository $commentrepo, EntityManagerInterface $manager,DaPostrepository $post,$id,Request $request)
   {
   		
   		$commentrepo = $commentrepo->find($id);
      $comments->setIsApprouved(1);
      $manager->flush();
      $msg  = null;
   		$msg = $request->getSession()->getFlashBag()->add('success_comment','commentaires approuver avec succès');

            return $this->redirectToRoute('readComment',['id' => $commentrepo->getPost()->getId(),'msg' => $msg]);
   }
   /**
     * @Route("/approuve_post", name="approuve_post")
     */
   public function adminPost(DaPostRepository $post)
   {
      $post = $post->findBy(['isApprouved' => 0]);
      return $this->render('admin/notification/article.html.twig',['posts' => $post , 'message' => $this->message,'nbr_comm_non_approuve' => $this->nbrComment,'nbr_post_non_approuve' => $this->nbrPost,'nbr_message_non_lu' => $this->nbrMessage]);
   }
   /**
     * @Route("/approuved_post/{id<[0-9]+>}", name="approuved_post")
     */
   public function approuvePost(DaPostRepository $post, $id , EntityManagerInterface $manager,Request $request)
   {
      $post = $post->find($id);
      $post->setIsApprouved(1);
      $manager->flush();
      $msg = null;
      $msg = $request->getSession()->getFlashBag()->add('success_post','articles approuver avec succès');
            
              return $this->redirectToRoute('list_post',['msg' => $msg]);
   }

   /**
     * @Route("/message_nousContacter", name="msgNousContacter")
     */
   public function msgNousContacter(NousContacterRepository $nousContacter,Request $request)
   {
      if($request->getSession()->getFlashBag()->has('success_message')){
        $msg = $request->getSession()->getFlashBag()->get('success_message')[0];
      }
      else{
         $msg = null;
      }
      $messages = $nousContacter->findBy(['isDeleted' => 0]);
      return $this->render('admin/notification/message.html.twig',['messages' => $messages ,'message' => $this->message,'nbr_comm_non_approuve' => $this->nbrComment,'nbr_post_non_approuve' => $this->nbrPost,'nbr_message_non_lu' => $this->nbrMessage,'msg' => $msg]);
   }

   /**
     * @Route("/messageNousContacter/{id<[0-9]+>}", name="msgNousContacterView")
     */
   public function msgNousContacterView(NousContacter $nousContacter, EntityManagerInterface $em)
   {
      $nousContacter->setIsView(1);
      $em->persist($nousContacter);
      $em->flush();
      return $this->render('admin/notification/message_view.html.twig',['messages' => $nousContacter ,'message' => $this->message,'nbr_comm_non_approuve' => $this->nbrComment,'nbr_post_non_approuve' => $this->nbrPost,'nbr_message_non_lu' => $this->nbrMessage]);
   }

    /**
     * @Route("/phpinfo", name="easyadmin_phpinfo")
     */
    public function phpInfoAction(): Response
    {
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }
        ob_start();
        phpinfo();
        $str = ob_get_contents();
        ob_get_clean();

        return new Response($str);
    }

   /**
     * @Route("/delete/{id<[0-9]+>}", name="delete_message")
     */
   public function deletemsgNousContacterView(NousContacter $nousContacter, EntityManagerInterface $em,Request $request)
   {
      $nousContacter->setIsDeleted(1);
      $em->persist($nousContacter);
      $em->flush();

      $msg = $request->getSession()->getFlashBag()->add('success_message','messages supprimer avec succès');
      return $this->redirectToRoute('msgNousContacter',['msg' => $msg]);
   }

    /**
     * @Route("/admin-journalist", name="list-journalist")
     */
    public function journalist(DaUserRepository $users,Request $request): Response
    {


        $journalist = $users->getJournalist();


        return $this->render('admin/journalist/journalist-list.php',[ 'message' => $this->message,'nbr_comm_non_approuve' => $this->nbrComment,'nbr_post_non_approuve' => $this->nbrPost,'nbr_message_non_lu' => $this->nbrMessage, 'journalist' => $journalist, 'msg' => '' ]);
    }

}