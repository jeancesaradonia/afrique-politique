<?php

namespace App\Controller;

use App\Repository\DaPostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/recherche", name="app_da_search")
     */
    public function index(Request $request, DaPostRepository $daPostRepository, PaginatorInterface $paginatorInterface): Response
    {

        $posts = $paginatorInterface->paginate(
            $daPostRepository->search($request->query->get('s', '')),
            $request->query->get("page", 1),
            10
        );
        return $this->render('front/post/search.html.twig', [
            'posts' => $posts,
            'wordKey' => $request->query->get('s', '')
        ]);
    }
}
