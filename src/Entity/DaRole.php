<?php

namespace App\Entity;

use App\Repository\DaRoleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DaRoleRepository::class)
 * @ORM\Table(name="da_user_role")
 */
class DaRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $roleName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $roleDescription;

    /**
     * @ORM\OneToMany(targetEntity=DaUser::class, mappedBy="daRole", orphanRemoval=true)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=DaUser::class, mappedBy="daRoles")
     */
    private $daUsers;

    

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->daUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoleName(): ?string
    {
        return $this->roleName;
    }

    public function setRoleName(string $roleName): self
    {
        $this->roleName = $roleName;

        return $this;
    }

    public function getRoleDescription(): ?string
    {
        return $this->roleDescription;
    }

    public function setRoleDescription(?string $roleDescription): self
    {
        $this->roleDescription = $roleDescription;

        return $this;
    }

    /**
     * @return Collection|DaUser[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(DaUser $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addDaRole($this);
        }

        return $this;
    }

    public function removeUser(DaUser $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getDaRoles() === $this) {
                $user->addDaRole(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DaUser[]
     */
    public function getDaUsers(): Collection
    {
        return $this->daUsers;
    }

    public function addDaUser(DaUser $daUser): self
    {
        if (!$this->daUsers->contains($daUser)) {
            $this->daUsers[] = $daUser;
            $daUser->addDaRole($this);
        }

        return $this;
    }

    public function removeDaUser(DaUser $daUser): self
    {
        if ($this->daUsers->removeElement($daUser)) {
            $daUser->removeDaRole($this);
        }

        return $this;
    }

    public function getDaSubscription(): ?DaSubscription
    {
        return $this->daSubscription;
    }

    public function setDaSubscription(?DaSubscription $daSubscription): self
    {
        // unset the owning side of the relation if necessary
        if ($daSubscription === null && $this->daSubscription !== null) {
            $this->daSubscription->setSubscriptionRole(null);
        }

        // set the owning side of the relation if necessary
        if ($daSubscription !== null && $daSubscription->getSubscriptionRole() !== $this) {
            $daSubscription->setSubscriptionRole($this);
        }

        $this->daSubscription = $daSubscription;

        return $this;
    }
}