<?php

namespace App\Service\MetierManagerBundle\Utils;

/**
 * Class EntityName
 * Classe qui contient les noms constante de tout les entités
 */
class EntityName
{
    const AS_COUNTRY = 'MetierManagerBundle:DaCountry';
    const AS_POST = 'MetierManagerBundle:DaPost';
}