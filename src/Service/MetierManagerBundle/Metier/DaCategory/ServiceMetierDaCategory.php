<?php

namespace App\Service\MetierManagerBundle\Metier\DaCategory;

use App\Entity\DaPost;
use App\Entity\DaPostCategory;
use App\Repository\DaPostCategoryRepository;
use App\Service\MetierManagerBundle\Utils\ServiceName;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use function Symfony\Component\String\u;

/**
 * Class ServiceMetierDaPostCategory
 * @package App\Service\MetierManagerBundle\Metier\DaPostCategory
 */
class ServiceMetierDaCategory
{
    private $_entity_manager;
    private $_container;
    private $_helper;
    private $_cacheManager;

    /**
     * ServiceMetierDaPostCategory constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     */
    public function __construct(EntityManagerInterface $_entity_manager, ContainerInterface $_container, UploaderHelper $helper, CacheManager $cacheManager)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
        $this->_helper          = $helper;
        $this->_cacheManager    = $cacheManager;
    }


    /**
     * post categories
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAllCategories()
    {
        $_postCategory = DaPostCategory::class;
        $_post = DaPost::class;

        $_dql = "SELECT ctg FROM $_postCategory ctg ";

        $_query = $this->_entity_manager->createQuery($_dql);
        $categories = [];
        foreach ($_query->getResult() as $key => $value) {
            $categories[$key]['entity'] = $value;
            $categories[$key]['nb_posts'] = count($value->getDaPosts());
        }
        return $categories;
    }

    /**
     * count post
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countPost()
    {
        $_post = DaPostCategory::class;

        $_dql = "SELECT COUNT (pst) as nbTotal
 					FROM $_post pst";

        $_query = $this->_entity_manager->createQuery($_dql);

        return $_query->getOneOrNullResult()['nbTotal'];
    }

    /**
     * update post meta
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function updatePostMeta($id, $newContent)
    {
        $_post = DaPostCategory::class;

        $_dql = "UPDATE $_post pst SET pst.metaDescription = :new_content
 				 WHERE pst.id = :pst_id	";

        $_query = $this->_entity_manager->createQuery($_dql);
        $_query->setParameter('pst_id', $id);
        $_query->setParameter('new_content', $newContent);
        return $_query->execute();
    }
}
