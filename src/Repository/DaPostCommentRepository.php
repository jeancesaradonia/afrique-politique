<?php

namespace App\Repository;

use App\Entity\DaPostComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaPostComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaPostComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaPostComment[]    findAll()
 * @method DaPostComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaPostCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaPostComment::class);
    }

    // /**
    //  * @return DaPostComment[] Returns an array of DaPostComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaPostComment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}