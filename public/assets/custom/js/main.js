/**
 * Javascript général
 */

$(function() {
  // Plugins select2
  $("#ad_post_categories, #da_admin_account_daRoles").select2();
  $(".categorie-select").select2();
  $('.select2-container').css('display', 'block');
});
