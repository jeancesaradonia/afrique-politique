<?php

namespace App\Service\MetierManagerBundle\Metier\DaAccount;

use App\Entity\DaUser;
use App\Service\MetierManagerBundle\Utils\ServiceName;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceMetierDaAccountAdmin
 * @package App\Service\MetierManagerBundle\Metier\DaAccountAdmin
 */
class ServiceMetierDaAccount
{
    private $_entity_manager;
    private $_container;

    /**
     * ServiceMetierDaAccountAdmin constructor.
     * @param EntityManagerInterface $_entity_manager
     * @param ContainerInterface $_container
     */
    public function __construct(EntityManagerInterface $_entity_manager, ContainerInterface $_container)
    {
        $this->_entity_manager = $_entity_manager;
        $this->_container      = $_container;
    }

    /**
     * Ajout user
     * @param DaUser $user
     * @param Object $_form
     * @return DaUser
     */
    public function addAccount($user, $_form, $request, $pwd_encode, $roles = null)
    {
        // Récupérer manager
        $utils_manager = $this->_container->get(ServiceName::SRV_METIER_UTILS);
        if (!empty($roles)) {
            foreach ($roles as $role) {
                $user->addDaRole($role);
            }
        }

        // Traitement du photo
        /*$_img_photo = $_form['usrPhoto']->getData();
        if ($_img_photo) {
            $_user_upload_manager = $this->_container->get(ServiceName::SRV_METIER_USER_UPLOAD);
            $_user_upload_manager->upload($user, $_img_photo);
        }
        */
        $pwd = $request->request->get('da_admin_account')['userPassword'];
        $user->setUserPassword($pwd_encode->encodePassword($user, $pwd));
        $user->setCreatedAt(new \DateTime());
        $user->setConfirmAt(new \DateTime());
        return $utils_manager->saveEntity($user, 'new');
    }

    /**
     * Modification user
     * @param DaUser $user
     * @param Object $_form
     * @return DaUser
     */
    public function updateAccount($user, $_form)
    {
        // Récupérer manager
        $_utils_manager = $this->_container->get(ServiceName::SRV_METIER_UTILS);
        $_seo_manager   = $this->_container->get(ServiceName::SRV_METIER_SEO);

        // Traitement image seo
        $_image_seo = $_form['csnSeo']['seoImageUrl']->getData();
        // S'il y a un nouveau fichier ajouté, on supprime l'ancien fichier puis on enregistre ce nouveau
        if ($_image_seo) {
            $_seo = $user->getCsnSeo();
            $_seo_manager->deleteOnlyImage($_seo);
            $_seo_manager->addImage($_seo, $_image_seo);
        }

        return $_utils_manager->saveEntity($user, 'new');
    }

    /**
     * post categories
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAllUsers()
    {
        $_user = DaUser::class;

        $_dql = "SELECT usr FROM $_user usr";

        $_query = $this->_entity_manager->createQuery($_dql);
        $categories = [];
        foreach ($_query->getResult() as $key => $value) {
            $categories[] = $value['categoryTitle'];
        }
        return $categories;
    }
}
