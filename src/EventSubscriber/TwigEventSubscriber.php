<?php

namespace App\EventSubscriber;

use Twig\Environment;
use App\Repository\DaPostRepository;
use App\Repository\DaPostCategoryRepository;
use App\Service\Post;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $daPostCategoryRepository;
    private $daPostRepository;
    private $post;
    public function __construct(Environment $twig,DaPostCategoryRepository $daPostCategoryRepository, DaPostRepository $daPostRepository, Post $post)
    {
        $this->twig = $twig;
        $this->daPostCategoryRepository = $daPostCategoryRepository;
        $this->daPostRepository = $daPostRepository;
        $this->post = $post;
    }
    public function onControllerEvent(ControllerEvent  $event)
    {
        $this->twig->addGlobal('flashNews', $this->daPostRepository->getFlashNews());
        $this->twig->addGlobal('alaunes', $this->daPostRepository->get("a-la-une", 5));
        $this->twig->addGlobal('cultures', $this->daPostRepository->get("culture", 5));
        $this->twig->addGlobal('archives', $this->post->getAllDateAvalableArchive());
        $this->twig->addGlobal('readMore', $this->daPostRepository->findBy([],['postCreatedAt' => 'DESC'], 4));
    }
   
    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
        
    }
}
