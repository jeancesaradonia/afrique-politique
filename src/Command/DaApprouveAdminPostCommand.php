<?php

namespace App\Command;

use App\Service\Post;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DaApprouveAdminPostCommand extends Command
{
    private Post $post;
    protected static $defaultName = 'da:approuve-post';
    public function __construct(Post $post)
    {
        $this->post = $post;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->post->getPostsToApprouve() as $postToApprouve) {
            $this->post->approuvePost($postToApprouve);
        }
        return Command::SUCCESS;
    }
}
