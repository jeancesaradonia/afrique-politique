<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ReadExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
           // new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('read_time', [$this, 'calculateReadTime']),
        ];
    }

    public function calculateReadTime($value)
    {
        $total_word = str_word_count(strip_tags($value));
        $m = floor($total_word / 230);
        $s = floor($total_word % 230 / (230 / 60));
        $estimateTime = $m . ' minute' . ($m == 1 ? '' : 's') . ', ' . $s . ' second' . ($s == 1 ? '' : 's');

        return $estimateTime;
    }
}
