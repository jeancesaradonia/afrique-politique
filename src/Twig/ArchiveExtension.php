<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ArchiveExtension extends AbstractExtension
{
    private $monthNamesFr;
    public function __construct()
    {
        $this->monthNamesFr = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
    }
    public function getFilters(): array
    {
        return [
           // new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_archive_date_full_written', [$this, 'getArchiveDateFullWritten']),
            new TwigFunction('get_archive_date_month', [$this, 'getArchiveDateMonth']),
            new TwigFunction('get_archive_date_year', [$this, 'getArchiveDateYear']),
        ];
    }

    public function getArchiveDateFullWritten($archive)
    {
        $archiveMY = explode(" ",$archive);
        $month = $archiveMY[1];
        return $this->monthNamesFr[$month - 1] . " " . $archiveMY[0];
    }

    public function getArchiveDateMonth($archive)
    {
        $archiveMY = explode(" ",$archive);
        return $archiveMY[1];
    }

    public function getArchiveDateYear($archive)
    {
        $archiveMY = explode(" ",$archive);
        return $archiveMY[0];
    }
}
