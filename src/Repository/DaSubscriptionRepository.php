<?php

namespace App\Repository;

use App\Entity\DaSubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaSubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaSubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaSubscription[]    findAll()
 * @method DaSubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaSubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaSubscription::class);
    }

    // /**
    //  * @return DaSubscription[] Returns an array of DaSubscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaSubscription
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}