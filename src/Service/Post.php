<?php

namespace App\Service;

use App\Entity\DaPost;
use App\Repository\DaPostRepository;
use Doctrine\ORM\EntityManagerInterface;

class Post{

    private DaPostRepository $daPostRepository;

    private EntityManagerInterface $entityManagerInterface;
    public function __construct(DaPostRepository $daPostRepository, EntityManagerInterface $entityManagerInterface)
    {
        $this->daPostRepository = $daPostRepository;
        $this->entityManagerInterface = $entityManagerInterface;
    }

    public function getAllDateAvalableArchive()
    {
        $dateExistingMY [] = [];
        foreach ($this->daPostRepository->findBy([], ["postCreatedAt" => "DESC"]) as $cle => $post) {
            $dateExistingMY [] = $post->getPostCreatedAt()->format("Y m");
        }
        $dateExistingMY = array_unique(array_filter($dateExistingMY));
        return $dateExistingMY;
    }

    public function getPostsToApprouve(): array
    {
        $posts = [];
        $now = new \Datetime('now');
        foreach ($this->daPostRepository->getPostNotApprouved() as $post) {
            // je vérifie les dates
            if ($post->getPostPublishedAt() && ($post->getPostPublishedAt() <= $now)) {
                array_push($posts, $post);
            }
        }
        return $posts;
    }

    public function approuvePost(DaPost $daPost)
    {
        $daPost->setIsApprouved(true);
        $this->entityManagerInterface->persist($daPost);
        $this->entityManagerInterface->flush();
    }



}