<?php

namespace App\Repository;

use App\Entity\DaCountry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DaCountry|null find($id, $lockMode = null, $lockVersion = null)
 * @method DaCountry|null findOneBy(array $criteria, array $orderBy = null)
 * @method DaCountry[]    findAll()
 * @method DaCountry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DaCountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DaCountry::class);
    }

    // /**
    //  * @return DaCountry[] Returns an array of DaCountry objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DaCountry
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}